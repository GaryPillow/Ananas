/*
 ****************************************************************************
 *  Copyright (c) 2015 Dark Guan <tickel.guan@gmail.com>                    *
 *	This file is part of Ananas.                                            *
 *                                                                          *
 *  Ananas is free software: you can redistribute it and/or modify          *
 *  it under the terms of the GNU General Public License as published by    *
 *  the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                     *
 *                                                                          *
 *  Ananas is distributed in the hope that it will be useful,              *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *  GNU General Public License for more details.                            *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with Ananas.  If not, see <http://www.gnu.org/licenses/>.       *
 ****************************************************************************
 */

/*
 * Encoder.h
 *
 *  Created on: 2015��12��8��
 *      Author: Dark
 */

#ifndef ENCODER_H_
#define ENCODER_H_

extern volatile bool encodecounton;
extern volatile long encodercountfeedrate;
extern volatile unsigned long absencodercounterfeedrate;

extern volatile long encodercount;

extern volatile uint16_t endertostep;

extern volatile bool inverseCountDir;

extern unsigned char subdivide;

extern unsigned char speedfactor;

//extern volatile unsigned long time;
//extern volatile unsigned long lasttime;
extern volatile unsigned long deltatime;

void initialEncoder();



void AMSpeedcalculate();


void refresh_endertostep();

float getPosition();

void setincrement();
void changeDir();

long getEncodercount();

void speed_calculate();

void setEncodercount(long count);

#endif /* ENCODER_H_ */
