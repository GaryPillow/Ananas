/*
 TLC5615.cpp - Arduino library support for the Digital-to-Analog conversion chip TLC5615
 Created by Luffy, May 1,2014.
 Released into the public domain.
 ver 1.0 BATE

 2016.1.7 add fastio by dark

 */

#include "Arduino.h"
#include "TLC5615.h"
#include "configuration.h"

TLC5615::TLC5615(uint8_t DIN, uint8_t SCLK, uint8_t CS) {
	pinMode(DIN, OUTPUT);
	pinMode(SCLK, OUTPUT);
	pinMode(CS, OUTPUT);
	DIN_Pin = DIN;
	SCLK_Pin = SCLK;
	CS_Pin = CS;
	InitTLC5615();

	//initial the voltage
}

void TLC5615::InitTLC5615() {
#ifdef FAST_IO
//	WRITE(CS_Pin, HIGH);
	WRITE(DA_CS, LOW);
	WRITE(DA_SCLK, LOW);
#else
	//	digitalWrite(CS_Pin, HIGH);
	digitalWrite(CS_Pin, LOW);
	digitalWrite(SCLK_Pin, LOW);
#endif
}

void TLC5615::DAConvert(uint16_t data) {
	InitTLC5615();
#ifdef FAST_IO
	for (unsigned char i = 0; i < 12; i++) {
		if (data & 0x200)
			WRITE(DA_DIN, HIGH);
		else
			WRITE(DA_DIN, LOW);
		WRITE(DA_SCLK, HIGH);
		data = data << 1;
		WRITE(DA_SCLK, LOW);
	}
	WRITE(DA_CS, HIGH);
	WRITE(DA_SCLK, LOW);
#else
	for (unsigned char i = 0; i < 12; i++) {
		if (data & 0x200)
		digitalWrite(DIN_Pin, HIGH);
		else
		digitalWrite(DIN_Pin, LOW);
		digitalWrite(SCLK_Pin, HIGH);
		data = data << 1;
		digitalWrite(SCLK_Pin, LOW);
	}
	digitalWrite(CS_Pin, HIGH);
	digitalWrite(SCLK_Pin, LOW);
#endif

}


