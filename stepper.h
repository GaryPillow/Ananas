/*
 ****************************************************************************
 *  Copyright (c) 2015 Dark Guan <tickel.guan@gmail.com>                    *
 *	This file is part of Ananas.                                            *
 *                                                                          *
 *  Ananas is free software: you can redistribute it and/or modify          *
 *  it under the terms of the GNU General Public License as published by    *
 *  the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                     *
 *                                                                          *
 *  Ananas is distributed in the hope that it will be useful,               *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *  GNU General Public License for more details.                            *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with Ananas.  If not, see <http://www.gnu.org/licenses/>.        *
 ****************************************************************************
 */

/*
 * stepper.h
 *
 *  Created on: 2015��12��8��
 *      Author: Dark
 */
#include "configuration.h"
#include "Ananas.h"

#ifndef STEPPER_H_
#define STEPPER_H_

//extern unsigned long mindelaytime;
//extern unsigned long normal_delaytime;
extern volatile long pulseCount;

extern volatile bool is_dir_connectted;

extern volatile bool stepcountdir;

void stepastep();

//extern float destination/;

void initialStepper();

//void changeMotorDir();

void changeStepCounterdir();

void step(bool dir);

void mapThepin();

float getDestination();

long getSteps();

void setSteps(long count);

long getError();

void printOUTstate();

//#ifdef SIMPLE
//void managerDelay();
//
//void managerDalaytime(long error);
//#endif

void changeDisableMotor();

#endif /* DA_H_ */

