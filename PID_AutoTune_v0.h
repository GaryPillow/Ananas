#ifndef PID_AutoTune_v0
#define PID_AutoTune_v0
#define PID_AutoTune_LIBRARY_VERSION	0.0.1

class PID_ATune
{


  public:
  //commonly used functions **************************************************************************
    PID_ATune(long*, long*);                       	// * Constructor.  links the Autotune to a given PID
    int Runtime();						   			   	// * Similar to the PID Compue function, returns non 0 when done
	void Cancel();									   	// * Stops the AutoTune	
	
	void SetOutputStep(long);						   	// * how far above and below the starting value will the output step?
	double GetOutputStep();							   	// 
	
	void SetControlType(int); 						   	// * Determies if the tuning parameters returned will be PI (D=0)
	int GetControlType();							   	//   or PID.  (0=PI, 1=PID)			
	
	void SetLookbackSec(int);							// * how far back are we looking to identify peaks
	int GetLookbackSec();								//
	
	void SetNoiseBand(long);							// * the autotune will ignore signal chatter smaller than this value
	double GetNoiseBand();								//   this should be acurately set
	
	double GetKp();										// * once autotune is complete, these functions contain the
	double GetKi();										//   computed tuning parameters.  
	double GetKd();										//
	
  private:
    void FinishUp();
	bool isMax, isMin;
	long *input, *output;
	long setpoint;
	long noiseBand;
	int controlType;
	bool running;
	unsigned long peak1, peak2, lastTime;
	unsigned int sampleTime;
	int nLookBack;
	int peakType;
	long lastInputs[101];
    long peaks[10];
	int peakCount;
	bool justchanged;
	bool justevaled;
	long absMax, absMin;
	long oStep;
	long outputStart;
	double Ku, Pu;
	
};
#endif

