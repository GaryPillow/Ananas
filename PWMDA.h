/*
 * PWMDA.h
 *
 *  Created on: 2016��5��31��
 *      Author: Dark
 */
#include "configuration.h"

#ifndef PWMDA_H_
#define PWMDA_H_

#ifdef PWMDA

void initialPWMDA();

void setVoltate(uint8_t vol);

uint8_t getVoltage();

#endif

#endif /* PWMDA_H_ */
