/*
 ****************************************************************************
 *  Copyright (c) 2015 Dark Guan <tickel.guan@gmail.com>                    *
 *	This file is part of Ananas.                                            *
 *                                                                          *
 *  Ananas is free software: you can redistribute it and/or modify         *
 *  it under the terms of the GNU General Public License as published by    *
 *  the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                     *
 *                                                                          *
 *  Ananas is distributed in the hope that it will be useful,              *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *  GNU General Public License for more details.                            *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with Ananas.  If not, see <http://www.gnu.org/licenses/>.       *
 ****************************************************************************
 */
/*
 * stepper.cpp

 *
 *  Created on: 2015年12月14日
 *      Author: Dark
 */

#include "Arduino.h"
#include "Ananas.h"
//#include "QUN.h"
#include "Encoder.h"
#ifdef USE_TLC5615 //no use TLC5615
#include "Da.h"
#endif
#include "language.h"
#include "stepper.h"

//#include "speed_lookuptable.h"

#include "con_configuration.h"

//#include "PinChangeInt.h"



#define M_CHANGE 0
#define M_RISING 2
#define M_FALLING 3

volatile long pulseCount;
//volatile long pulseCount_T;

//static long encoderCount;

//long error;

bool haserror = false; //whether there is error
uint16_t errorAbs;

bool dirState;
bool enalbeState;

bool invDir;

bool FOLLOW;

//static unsigned long lastvoltagetime = 0;
//static unsigned long maxvoltagetime = 0;
//static unsigned long voltagechangetime = 60 * 1000;

//#ifdef SIMPLE
//简单 算法 （测试算法）

//unsigned long normal_delaytime = DELAYTIME;
//unsigned long mindelaytime = MIN_DELAYTIME;

bool enableDelay = false;
uint16_t delaytime;

unsigned long pursueFeedrate;

//wether connect in and out signal directly
volatile bool is_dir_connectted;
volatile bool stepcountdir;
//
//#endif

volatile bool pulseon; //true means there is pulse flowing in;
//false means there is no pulse flowing in;

//使用比较器中断
//QUN M;

//define
//void pulse();
void stepastep();
//void managerDalaytime(long error);
//void mapThepin();
//
//FORCE_INLINE void PCINT_pulse() {
//	noInterrupts();
////	Serial.println("D7 RUN");
//	dirState = READ(DIRPIN);
////	WRITE(13, dirState);
////	输出脉冲
//	(dirState ^ stepcountdir) ? pulseCount++ : pulseCount--;
//	pulseon = true;
//	interrupts();
//}
//FORCE_INLINE void PCINT_pulse_none() {
//	noInterrupts();
////	Serial.println("D7 RUN");
//	dirState = READ(DIRPIN);
////	WRITE(13, dirState);
////	输出脉冲
////	(dirState ^ stepcountdir) ? pulseCount++ : pulseCount--;
//	pulseon = true;
//	interrupts();
//}

void initialStepper() {
	//set input pins
	//在这里要用到光耦的时候 需要上拉 电阻
	SET_INPUT(DIRPIN);
	SET_INPUT(STEPPIN);

#if	DEVICE == DIY_TEST
	SET_INPUT(ENABLEPIN);
	SET_OUTPUT(STEP_ENABLE);
#endif
	//set output pins
	SET_OUTPUT(STEP_DIR);
	SET_OUTPUT(STEP_STEP);

//	SET_OUTPUT(13);

	//初始化锁紧电机
//	digitalWrite(STEP_ENABLE, LOW);
	//把step out端口置为高位
	WRITE(STEP_STEP, HIGH);
	//close motor
	WRITE(STEP_ENABLE, LOW);

	//把step in端口置为高位 这里要匹配上位机，如果是光耦隔离 就不需要
	//2015/12/26 添加 光耦测试，输入引脚全部拉高
#ifdef OPTOCOUPLE
	WRITE(STEPPIN, HIGH);
	WRITE(DIRPIN, HIGH);
#if	DEVICE == DIY_TEST
	WRITE(ENABLEPIN, HIGH);
#endif

#else
	WRITE(STEPPIN, HIGH);
#endif

	pinMode(MAXENDSTOP,INPUT);
	pinMode(MINENDSTOP,INPUT);

#if ENDSTOPPULLUP_MAX ==1
//	digitalWrite(MAXENDSTOP, HIGH);
#endif
#if ENDSTOPPULLUP_MIN ==1
//	digitalWrite(MINENDSTOP, HIGH);
#endif

//	WRITE(13, HIGH);
	//add interrupt
	//TODO 需要结合上位机判断，脉冲初始的是高还是低电平

//	M.attachInterrupt(pulse, M_FALLING); //D7

	//对下降沿计数
	// M_CHANGE 当引脚电平发生改变时，触发中断
	// M_RISING 当引脚由低电平变为高电平时，触发中断
	// M_FALLING 当引脚由高电平变为低电平时，触发中断
	pulseCount = 0;
	//
	invDir = INV_DIR;
//#ifdef SIMPLE
//	delaytime = DELAYTIME;
//
//	//initial with connected directly
//	is_dir_connectted = true;
//#endif

//	mapThepin();

	//初始化没有脉冲输入
	pulseon = false;
//********************************
	unsigned char bytes = M_FALLING;
	//D7 interrupt
	PORTD |= _BV(PORTD7);
//	gao = f;
	DIDR1 |= _BV(AIN1D) | _BV(AIN0D);
	ACSR = 0;
	ACSR |= _BV(ACIE) | _BV(ACBG) | bytes;
	SREG |= (1 << 7);
//********************************
//	attachPinChangeInterrupt(STEPPIN, PCINT_pulse, FALLING);
//	attachPinChangeInterrupt(STEPPIN, PCINT_pulse_none, FALLING);
}

//**********************
ISR(ANALOG_COMP_vect) {
//  (*gao)();
	noInterrupts();
//	Serial.println("D7 RUN");
	dirState = READ(DIRPIN);
//	WRITE(13, dirState);
//	输出脉冲
	(dirState ^ stepcountdir) ? pulseCount++ : pulseCount--;
//	(dirState ^ stepcountdir) ? pulseCount_T++ : pulseCount_T--;
//	if (dirState) {
//		pulseCount++;
//		//		stepastep();
//		if (is_dir_connectted) {
//			WRITE(STEP_ENABLE, LOW);
//			WRITE(STEP_DIR, invDir);
//			WRITE(STEP_STEP, LOW);
//			WRITE(STEP_STEP, HIGH);
//		}
//	} else {
//		pulseCount--;
//		//		stepastep();
//		if (is_dir_connectted) {
//			WRITE(STEP_ENABLE, LOW);
//			WRITE(STEP_DIR, !invDir);
//			WRITE(STEP_STEP, LOW);
//			WRITE(STEP_STEP, HIGH);
//		}
//	}
	pulseon = true;
	interrupts();
}
//**************************************

void changeStepCounterdir() {
	stepcountdir = !stepcountdir;
}
////只在调试的时候使用
//void changeMotorDir() {
//	invDir = !invDir;
//}

//void pulse() {
////对上位机脉冲进行计数
////同时实时发生给步进驱动
//	noInterrupts();
//	WRITE(STEP_ENABLE, LOW);
//	dirState = READ(DIRPIN);
//	//输出脉冲
//	if (dirState) {
//		pulseCount++;
////		stepastep();
//		if (is_dir_connectted) {
//			WRITE(STEP_DIR, invDir);
//			WRITE(STEP_STEP, LOW);
//			WRITE(STEP_STEP, HIGH);
//		}
//	} else {
//		pulseCount--;
////		stepastep();
//		if (is_dir_connectted) {
//			WRITE(STEP_DIR, !invDir);
//			WRITE(STEP_STEP, LOW);
//			WRITE(STEP_STEP, HIGH);
//		}
//	}
//	pulseon = true;
//	interrupts();
//}

//测试用 转动一圈 检测线路 用来测试电机方向和连线
//实际不使用
void step(bool dir) {
	WRITE(STEP_ENABLE, HIGH);
	if (dir) {
		WRITE(STEP_DIR, !invDir);
	} else {
		WRITE(STEP_DIR, invDir);
	}
	for (int i = 0; i < (MOTOR_STEPS_PER_CIRCLE * DRIVE_SUBDIVE); i++) {
//		stepastep();
		WRITE(STEP_STEP, LOW);
		WRITE(STEP_STEP, HIGH);
		delayMicroseconds(60000 / 2);
	}
}

//主要来处理ENABLEPIN
//void mapThepin() {
//	enalbeState = READ(ENABLEPIN);
//	WRITE(STEP_ENABLE, enalbeState);
//}

float getDestination() {
	return float(pulseCount / float(STEPS_PER_UNIT));
}

long getSteps() {
	return pulseCount;
}

void setSteps(long count) {
	pulseCount = count;
}

//long getError() {
//	return error;
//}

//void managerVoltageBytime() {
//	if (encodecounton) {
//		lastvoltagetime = micros();
//	} else {
//		maxvoltagetime = micros() - lastvoltagetime;
//	}
//}

//#ifdef SIMPLE //SIMPLE algorithem
////manager the error between pulse count and encoder count
//void managerDelay() {
//	encoderCount = getEncodercount();
//	error = (encoderCount * STRONG_PURSE_FACTOR - pulseCount);
//	errorAbs = abs(error);
////
//	if (errorAbs < STRONG_PURSE_FACTOR)
//		return;
////manage the voltage
//	managerVoltage(errorAbs);
//
//	managerDalaytime(errorAbs);
//
//	if (errorAbs >= STRONG_PURSE_FACTOR) {
//
//		WRITE(STEP_ENABLE, LOW);
//		if (error > 0) {
//			WRITE(STEP_DIR, !invDir);
////			stepastep();
//			for (unsigned char i = 0; i < step_loops; i++) {
//				noInterrupts();
//				WRITE(STEP_STEP, LOW);
//				WRITE(STEP_STEP, HIGH);
//				interrupts();
//			}
//		} else {
//			WRITE(STEP_DIR, invDir);
////			stepastep();
//			for (unsigned char i = 0; i < step_loops; i++) {
//				noInterrupts();
//				WRITE(STEP_STEP, LOW);
//				WRITE(STEP_STEP, HIGH);
//				interrupts();
//			}
//		}
//		interrupts();
//		if (enableDelay)
//			delayMicroseconds(delaytime);
//	}
////	delayMicroseconds(1000);
//
//}
//
//void managerDalaytime(long error) {
//	if (error < (MAXERROR)) {
//		enableDelay = true;
//		is_dir_connectted = true;
////		delaytime = calc_timer(absencodercounterfeedrate);	/// 2;
////		if (delaytime > normal_delaytime)
////			delaytime = normal_delaytime;
//
//		delaytime = normal_delaytime;
//		step_loops = 1;
//	} else {
//		enableDelay = true;
//
////		if (absencodercounterfeedrate > 1000)
////			delaytime = mindelaytime;	//满足
////		else {
////			delaytime = calc_timer(absencodercounterfeedrate);	/// 2
////			if (delaytime > normal_delaytime)
////				delaytime = normal_delaytime;
////		}
//		delaytime = calc_timer(absencodercounterfeedrate / 2);	/// 2;
////		if (pulseon) {
////
////			if (absencodercounterfeedrate > 300) {
////				delaytime = MIDDELAYTIME;
////			}
////			if (delaytime > normal_delaytime) {
////				delaytime = normal_delaytime;
////			}
////
////		} else {
////			delaytime = mindelaytime;	//满足
////			if (absencodercounterfeedrate > 1000) {
////				delaytime = mindelaytime;	//满足
////
////			}
////		}
////		pulseon = false;
//		is_dir_connectted = false;
//		if (absencodercounterfeedrate > 100) {
//			delaytime = mindelaytime;	//满足
//		}
//	}
//}
//#endif

void changeDisableMotor() {
//	mapThepin();
	WRITE(STEP_ENABLE, HIGH);
}

void stepastep() {
	noInterrupts();
	WRITE(STEP_STEP, LOW);
	WRITE(STEP_STEP, HIGH);
	interrupts();
}
