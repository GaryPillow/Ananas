# Ananas
This is an Arduino based program for step motor close-loop controller,Ananas.  
  
如果你认为我做的这些对你来说是有价值的, 并鼓励我进行更多开源和免费的开发. 那你可以资助我, 就算是一杯咖啡... 
If you find my work useful and you want to encourage the development of more free resources, you can do it by donating...

<a rel="donate1" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8GQHVZ7YR8NZE">
<img alt="PalpayDonate" style="border-width:0" src="https://raw.githubusercontent.com/Dark-Guan/AnanasBoard/master/donate-with-paypal.png" />

  https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8GQHVZ7YR8NZE

<a rel="donate2" href="https://raw.githubusercontent.com/Dark-Guan/AnanasBoard/master/apcazu6ntbjy04py09.png">
<img alt="Alipay" style="border-width:0"  src="https://raw.githubusercontent.com/Dark-Guan/AnanasBoard/master/donate-with-alipay.png" />

  https://raw.githubusercontent.com/Dark-Guan/AnanasBoard/master/apcazu6ntbjy04py09.png

## Featrue

1. Quadrature Encoder Supported
2. Intelligent Tuning of the Motor Current
3. Up to 30Khz Pulse Input Supported
4. Up to 60Khz Stepper Driver PWM with Trapezoid Acceleration
5. Internal PID Speed Controller
6. PWM DA motor current control

## Version
- 2016.6.18  
Ananas 0.9.2 Support PWM DA for Motor Current Control

### note:
Ananas does not support AS5600 any more. Because a big bug with errors caused by Time Different of I2C communication.


## HardWare
### PCB
eagle ：  

https://github.com/Dark-Guan/AnanasBoard

## Discuss
### QQ Group
AnanasStepper 474390646  

### MAIL
tickel.guan@gmail.com

## Thanks

### Thank You for the library and code :(In no particular order)  


- Arduino eclipse extensions [http://eclipse.baeyens.it/](http://eclipse.baeyens.it/)
- Luffy For TLC5615 library http://www.arduino.cn/thread-5521-1-1.html http://tieba.baidu.com/p/3018247841
- mylife1213@gmail.com for QUN  http://www.geek-workshop.com/thread-11507-1-1.html
- neuroprod for https://github.com/neuroprod/ClosedLoopDriver/
- PID https://github.com/br3ttb/Arduino-PID-Library
- PID_AutoTune https://github.com/br3ttb/Arduino-PID-AutoTune-Library
- uStepper (AS5600)  https://github.com/uStepper/uStepper
- Arduino https://www.arduino.cc/
- Marlin https://github.com/ErikZalm/Marlin/

Necessary library is in the file, lib.zip. Please extract them into the "library" folder in Arduino installation directory.

This program is written with Arduino Eclipse Plug-in ,but you can run with arduino IDE.


### Thank You for individuals：(In no particular order)  
- 护林人
- 机智的小五
- 抚顺-醉爱罗马
- 太原 巨窝 
- 山东YAKE
- 上海-JOE
- 东莞-慢时光
- 溯 
- 贺牙
- 鸿见
- upccp
- 天津 tjnkwangjun
- 上海 qiush1234
- 广东 揭阳 elyoslim  
- 四川 泸州 tb898195_00  

## Future

1. Test As5601 , As5145B ,AS5304 and AS5311
2. Working On Stm32 Version.
