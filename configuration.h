/*
 ****************************************************************************
 *  Copyright (c) 2015 Dark Guan <tickel.guan@gmail.com>                    *
 *	This file is part of Ananas.                                        	*
 *                                                                          *
 *  Ananas is free software: you can redistribute it and/or modify          *
 *  it under the terms of the GNU General Public License as published by    *
 *  the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                     *
 *                                                                          *
 *  Ananas is distributed in the hope that it will be useful,               *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *  GNU General Public License for more details.                            *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with Ananas.  If not, see <http://www.gnu.org/licenses/>.        *
 ****************************************************************************
 */

/*
 * configuration.h
 *
 *  Created on: 2015年12月14日
 *      Author: Dark
 */

/*
 * Ananas Used and modified Some Marlin Code
 * "https://github.com/ErikZalm/Marlin/"
 */

#include "arduino.h"

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

//nano after The Nine Planets
#define MACHINENAME "Ananas Mercury"

// build by the user have been successfully uploaded into firmware.
#define STRING_VERSION_CONFIG_H __DATE__ " " __TIME__ // build date and time
#define STRING_CONFIG_H_AUTHOR "(Dark, Ananas)" // Who made the changes.

// EEPROM
// The microcontroller can store settings in the EEPROM, e.g. max velocity...
// M500 - stores parameters in EEPROM
// M501 - reads parameters from EEPROM (if you need reset them after you changed them temporarily).
// M502 - reverts to the default "factory settings".  You still need to store them in EEPROM afterwards if you want to.
//define this to enable EEPROM support
#define EEPROM_SETTINGS
//to disable EEPROM Serial responses and decrease program space by ~1700 byte: comment this out:
// please keep turned on if you can.
#define EEPROM_CHITCHAT

/*
 * control
 * pursue function needed parameter
 */



/*
 * PINS DIFINE
 * pins map for ananas
 */

//1 for liu 2 for mine and xiaowu 3 for 0.91
#define DIY_TEST 2
#define LIU 1
#define ANANAS091 3
#define ANANAS092 4

#define DEVICE ANANAS092//

#if DEVICE ==LIU

//test Ananans
#define DIRPIN 12
#define STEPPIN 7
#define ENABLEPIN 8

#define ENCODER_A 2//interrupt
#define ENCODER_B 3//interrupt
//for arduino's output
////TLC5615 DA
//#define DA_DIN 4//11
//#define DA_SCLK 5//10
//#define DA_CS 6//9
//test Ananans
#define STEP_DIR 5
#define STEP_STEP 13
#define STEP_ENABLE 4

#elif DEVICE == DIY_TEST
//total 22 pins
//for input
#define DIRPIN 8//12//6 2015.12.9
#define STEPPIN 7 //interrupt no changes
#define ENABLEPIN 9//8

#define ENCODER_A 2//interrupt
#define ENCODER_B 3//interrupt
//for arduino's output

//TLC5615 DA
#define DA_DIN 4//11
#define DA_SCLK 5//10
#define DA_CS 6//9

//stepper motor drive
#define STEP_DIR 12//5 //12
#define STEP_STEP 11//13
#define STEP_ENABLE 10// 4

#elif DEVICE == ANANAS091
#define DIRPIN 8//12//6 2015.12.9
#define STEPPIN 7 //interrupt no changes
//#define ENABLEPIN 9//8

#define ENCODER_A 2//interrupt
#define ENCODER_B 3//interrupt
//for arduino's output

//TLC5615 DA
//#define DA_DIN 12//11
//#define DA_SCLK 11//10
//#define DA_CS 10//9

//stepper motor drive
#define STEP_DIR 5//5 //12
#define STEP_STEP 4//13
#define STEP_ENABLE 6// 4

#elif DEVICE == ANANAS092
#define DIRPIN 8//12//6 2015.12.9
#define STEPPIN 7 //interrupt no changes

#define ENCODER_A 2//interrupt
#define ENCODER_B 9//3//interrupt
//for arduino's output

//PWM DA
#define PWMDA 3

//ADC
#define ADCPIN A0

//ENDSTOP
#define MAXENDSTOP 15 //A1
#define MINENDSTOP 16 //A2

//stepper motor drive
#define STEP_STEP 4//13
#define STEP_DIR 5//5 //12
#define STEP_ENABLE 6// 4

#define LED 13
//not support other function
//TODO Tube Nixie support

#endif

//***************************
//end of PINS MAP
#define BANDRATE 250000
//#define MYSERIAL Serial

//The ASCII buffer for receiving from the serial:
#define MAX_CMD_SIZE 32
#define BUFSIZE 2


/*
 * Maching Setting
 */

#define STEPS_PER_UNIT 100 //
#define MAXERROR 8//2//5//10 //
//***************************

/*
 * register the motor
 */
#define MOTOR_STEPS_PER_CIRCLE 200
//#define MOTOR_MAX_CURRENT 1.5 //A
//***************************
//end of motor register

//***************************

/*
 * register stepper motor driver
 */
#define INV_DIR false//


//***************************
// End of driver register

/*
 * register PWMDA
 */
#ifdef  PWMDA
#define DA_initial 100//
#define MAX_DA 255//300//280
#define DA_ADJUSTTINE 4000
#endif
//***************************
//end of DA register

/*
 * register DA
 */
#ifdef DA_MACHINE
#define DA_NAME TLC5615
#define DA_PRECISSION 1024 // V_ref/DA_PRECISSION
#define DA_initial 180//150//100//50//200//150//100
#define MAX_DA 300//300//280
#endif
//***************************
//end of DA register


#define DRIVE_SUBDIVE 16
/*
 * register ENCODER
 */
#define INV_COUNT_DIR true//false//true //
#define ENCODER_SUBDIVIDE 4

//***************************
//end of ENCODER register
/*
 * register ENCODER
 */
#define ENDSTOPPULLUP_MAX 1
#define ENDSTOPPULLUP_MIN 1

const bool MAX_ENDSTOP_INVERTING = false;
const bool MIN_ENDSTOP_INVERTING = false;
/*
 * Temprature
 */
#define R 4700.0 /**< The NTC resistor used for measuring temperature, is placed in series with a 4.7 kohm resistor. This is used to calculate the temperature */

/**
*	Coefficients needed by the Steinhart-hart equation in order to find the temperature of the NTC (and hereby the temperature of the motor driver)
*	from the current resistance of the NTC resistor. The coefficients are calculated for the following 3 operating points:
*
*	A: T = 5 degree Celsius
*
*	B: T = 50 degree Celsius
*
*	C: T = 105 degree Celsius
*
*	The Steinhart-Hart equation is described at the following link:
*
*	https://en.wikipedia.org/wiki/Steinhart%E2%80%93Hart_equation#Developers_of_the_equation
*/

//#define A 0.001295752996237
//#define B 0.000237488365866  /**< See description of A */
//#define C 0.000000083423218  /**< See description of A */

//100K NTC
#define A 0.000917960384403689
#define B 0.000192702800486336  /**< See description of A */
#define C 0.00000014454357540   /**< See description of A */

#define MaxTemp 80 //Celsius
#define TEMPDETECTTIME 8000 //8s

#endif /* CONFIGURATION_H_ */
