/*
 * PWMDA.cpp
 *
 *  Created on: 2016��5��31��
 *      Author: Dark
 */
#include "PWMDA.h"

#ifdef PWMDA

static uint8_t voltage;

void initialPWMDA() {
	voltage = 0;
	//Initial PWM
	//62Khz
#if PWMDA==3 //use port3
	pinMode(PWMDA, OUTPUT);
	TCCR2A = _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);
	TCCR2B = _BV(CS20);	//No prescaling
	OCR2B = 0;
#endif
}

void setVoltate(uint8_t vol) {
	voltage = vol;
#if PWMDA==3 //use port3
	OCR2B = voltage;
#endif
}

uint8_t getVoltage() {
	return voltage;

}

#endif

