/*
 * no_timer_digitalWrite.cpp
 *
 *  Created on: 2016��6��7��
 *      Author: Dark
 */

#include "wiring_private.h"
#include "pins_arduino.h"
#include "Ananas.h"

void digitalWriteNoTimer(uint8_t pin, uint8_t val)
{
	uint8_t bit = digitalPinToBitMask(pin);
	uint8_t port = digitalPinToPort(pin);
	volatile uint8_t *out;

	out = portOutputRegister(port);

	uint8_t oldSREG = SREG;
	cli();

	if (val == LOW) {
		*out &= ~bit;
	} else {
		*out |= bit;
	}

	SREG = oldSREG;
}
