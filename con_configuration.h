/*
 ****************************************************************************
 *  Copyright (c) 2015 Dark Guan <tickel.guan@gmail.com>                    *
 *	This file is part of Ananas.                                        	*
 *                                                                          *
 *  Ananas is free software: you can redistribute it and/or modify          *
 *  it under the terms of the GNU General Public License as published by    *
 *  the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                     *
 *                                                                          *
 *  Ananas is distributed in the hope that it will be useful,               *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *  GNU General Public License for more details.                            *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with Ananas.  If not, see <http://www.gnu.org/licenses/>.        *
 ****************************************************************************
 */
#include "configuration.h"

#ifndef CON_CONFIGURATION_H_
#define CON_CONFIGURATION_H_

/*
 * control
 * pursue function needed parameter
 */
//#define SIMPLE 0x01
//
//#ifdef SIMPLE
//#define DELAYTIME 60000//2000 //the end delay time 65535 is the max one for long
//#define MIDDELAYTIME 1000
//#define MIN_DELAYTIME 10//us
//#define PURSEFACTOR 10
//#define DELAY_FACTOR ((DELAYTIME-MIN_DELAYTIME)/(PURSEFACTOR*MAXERROR))
#define STRONG_PURSE_FACTOR 2//2//1.6//2//2//4 //this match the driver and motor

//#define SIMPLEP 10
//#define SIMPLEI 10
//#define SIMPLED 10

//#endif

//use PID controller
#define PIDcontroller 0x02

#ifdef PIDcontroller
#define KP 260
#define KI 0.01
#define KD 0

#define PIDTIMEUS 200//200 //us 5khz

#define PIDTIME 1 //ms
#define DEFAULT_MAX_FEEDRATE 60000 //50khz is the max frequency
#define USR_MAX_FEEDRATE 50000 //hz
#define DEFAULT_ACC_RATE 900000//300000 //steps//s^2 900000
#define JERK_FEEDRATE 1200//40//1000//2000 //steps/s

#define START_FEEDRATE 32//1500 //START_FEEDRATE should be bigger then JERK_FEEDRATE

#endif

#define SPEEDBASED

#ifdef SPEEDBASED
//speed measure time for interrupt
#define SPEEDMEASURETIME 10//100 //100ms mearure range
#define SPEEDUNIT (1000/SPEEDMEASURETIME) //encoder/s
#endif

#define OPTOCOUPLE //use OPTOCOUPLE

#define USEAMS5600

#define USE8825 //use 8825

#if DEVICE == ANANAS092
#define USEENDSTOP

#define TEMPDETECT //NTC

#define SPI_COM //SPI

#define SYCN //synchronization
#endif//DEVICE == ANANAS092

#endif //CON_CONFIGURATION_H_
