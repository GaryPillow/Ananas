/*
 ****************************************************************************
 *  Copyright (c) 2015 Dark Guan <tickel.guan@gmail.com>                    *
 *	This file is part of Ananas.                                        *
 *                                                                          *
 *  Ananas is free software: you can redistribute it and/or modify          *
 *  it under the terms of the GNU General Public License as published by    *
 *  the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                     *
 *                                                                          *
 *  Ananas is distributed in the hope that it will be useful,               *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *  GNU General Public License for more details.                            *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with Ananas.  If not, see <http://www.gnu.org/licenses/>.        *
 ****************************************************************************
 */

/*
 * controller.h
 *
 *  Created on: 2016��1��26��
 *      Author: Dark
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_
#include "configuration.h"

#define ENABLE_STEPPER_DRIVER_INTERRUPT()  TIMSK1 |= (1<<OCIE1A)
#define DISABLE_STEPPER_DRIVER_INTERRUPT() TIMSK1 &= ~(1<<OCIE1A)

extern volatile bool Max_Stop_state;
extern volatile bool Min_Stop_state;

extern double kp;
extern double ki;
extern double kd;
extern long error;

extern unsigned short PIDtime;

extern volatile bool controldir; //target motor dir
extern volatile float factor;
extern volatile float encoderfactor;

extern uint32_t max_feedrate; //user max feedrate
extern unsigned long acceleration_rate; //accelarate steps/s^2

extern unsigned short jerk_rate; //the rate to change dir steps/s need initial OCR1A  timer

extern unsigned short start_rate; //start rate eed initial OCR1A  timer

extern volatile unsigned short targertFeedrate; //target speed steps/s
extern volatile bool targertdir; //target motor dir

extern volatile unsigned short currentFeedrate; //current speed steps/s
extern volatile bool currentdir; //current motor dir

void PID_initial();

//PID tuning
//*********************************
int runTuning();

void SetTuning(bool);

void StopTuning();

void printTuningPID();

//PID tuning
//*********************************

//bool PID_compute();
void PID_compute();
//

void us_PID_Compute();

void us_PID_Compute_InCMT();

//void setfactor(float f);
void changecontroldir();

void refreshPID();

void refreshmaxfeedrate();
void setmaxfeedrate(unsigned long max);
void setaccelerationrate(unsigned long acc);
void setjerkfeedrate(unsigned short jerk);
void setstartfeedrate(unsigned short start);
void setlockfeedrate(unsigned short lock);

//for test
void setTarget_feedrate(unsigned short target_Rate); //steps/s

void change_targertdir();

void setPID(double kpin, double kiin, double kdin);

void setP(double kpin);
void setI(double kiin);
void setD(double kdin);

void printNewPID();
void printPID();

#endif /* CONTROLLER_H_ */
