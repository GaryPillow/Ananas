/*
 ****************************************************************************
 *  Copyright (c) 2015 Dark Guan <tickel.guan@gmail.com>                    *
 *	This file is part of Ananas.                                            *
 *                                                                          *
 *  Ananas is free software: you can redistribute it and/or modify          *
 *  it under the terms of the GNU General Public License as published by    *
 *  the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                     *
 *                                                                          *
 *  Ananas is distributed in the hope that it will be useful,               *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *  GNU General Public License for more details.                            *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with Ananas.  If not, see <http://www.gnu.org/licenses/>.        *
 ****************************************************************************
 */
/*
 * Encoder.cpp
 *
 *  Created on: 2015年12月14日
 *      Author: Dark
 */
#include "arduino.h"
#include "Ananas.h"
//#include "MsTimer2.h"
#include "Stepper.h"

#include "configuration.h"
#include "con_configuration.h"
#include "Encoder.h"

#include "AMS_5600.h"
#include "Wire.h"

#include "PinChangeInt.h"

#define FOUR_SUB 4
#define TWO_SUB 2
#define ONE_SUB 1

#ifdef USEAMS5600
//AMS
AMS_5600 ams5600;
#define MINICOUNT -2048
#define MAXCOUNT 2048
#define PRECISION 4096
//deaf zone
#define DeadZone1 512
#define DeadZone4 511
#define DeadZone5 513
#define DeadZone2 256
#define DeadZone3 0

volatile unsigned int Np = 0;
volatile unsigned int Lp = 0;
volatile int count = 0;
//volatile long countAll = 0;

#endif

//encoder use interrupt
volatile long encodercount;

//for speed calculate
long lastencodercount; //
volatile long encodercountfeedrate; //encoder rotate speed
volatile unsigned long absencodercounterfeedrate;

bool endcoderdir;

volatile bool encodecounton; //true means there is encodecount flowing in;
//false means there is encodecount flowing in;

volatile bool stateA;
volatile bool stateB;

volatile bool inverseCountDir;

volatile int8_t increment;

volatile uint16_t endertostep;

unsigned char subdivide;

unsigned char speedfactor;

//
//volatile unsigned long time;
//volatile unsigned long lasttime;
//volatile unsigned long deltatime;

void counter1_C();
void counter2_C();
void counterSub();

//void speedmeasure();
//void speedcalculate();

void initialEncoder() {
	//set all the pins
	pinMode(ENCODER_A, INPUT);
	pinMode(ENCODER_A, INPUT);
	//use the pull-up resistor
	digitalWrite(ENCODER_A, HIGH);
	digitalWrite(ENCODER_B, HIGH);

	//CHANGE

#ifndef USEAMS5600
	switch (subdivide) {
		case FOUR_SUB:
		SERIAL_ECHOLNPGM("Using FOUR_SUB");
		attachInterrupt(0, counter1_C, CHANGE); //为A 相添加中断
#if DEVICE == ANANAS092
		attachPinChangeInterrupt(ENCODER_B, counter2_C, CHANGE);
#else
		attachInterrupt(1, counter2_C, CHANGE); //为B 相添加中断
#endif
		break;
		case TWO_SUB:
		SERIAL_ECHOLNPGM("Using TWO_SUB");
		attachInterrupt(0, counterSub, CHANGE); //为A 相添加中断
		break;
		case ONE_SUB:
		SERIAL_ECHOLNPGM("Using ONE_SUB");
		attachInterrupt(0, counterSub, FALLING);//为A 相添加中断
		break;
		default:
		SERIAL_ECHOLNPGM("Not Using Optical Encoder!");
		break;
	}
#endif
	encodercount = 0;

	//TODO add EEPROM settings
//	inverseCountDir = INV_COUNT_DIR;
//	if (inverseCountDir) {
//		increment = 1;
//	} else {
//		increment = -1;
//	}

	//start speed measure
	encodecounton = false;
	lastencodercount = encodercount;

	absencodercounterfeedrate = 0;

//	MsTimer2::set(SPEEDMEASURETIME, speedcalculate);
//	MsTimer2::start(); //start timer

	//inital encoders/s to  steps/s
	//and /2 is prepare for speed lookup

//	endertostep = SPEEDUNIT * STRONG_PURSE_FACTOR / 2;

//	endertostep = SPEEDUNIT * STRONG_PURSE_FACTOR;
//	time = 0;

	//开启
#ifdef USEAMS5600
	Wire.begin();
	Np = ams5600.getRawAngle();
	Lp = Np;
#endif

}

#ifdef USEAMS5600
void AMSpeedcalculate() {
	Np = ams5600.getRawAngle();
//	if (Np == DeadZone4) {
//		Serial.println("5600Return4!");
//		return;
//	}
	count = Np - Lp;
	int absmp = abs(Np);
	if (absmp == DeadZone1) {
		Serial.println("5600Return1!");
		return;
	}
	if (absmp == DeadZone2) {
		Serial.println("5600Return2!");
		return;
	}
	if (absmp == DeadZone3) {
		Serial.println("5600Return3!");
		return;
	}
	if (absmp == DeadZone4) {
		Serial.println("5600Return4!");
		return;
	}
	if (absmp == DeadZone5) {
		Serial.println("5600Return5!");
		return;
	}

	Lp = Np;
	if ((count > MINICOUNT) && (count < MAXCOUNT)) {
		inverseCountDir ? encodercount += count : encodercount -= count;
	} else if (count >= MAXCOUNT) {
		inverseCountDir ?
				encodercount += PRECISION - count :
				encodercount -= PRECISION - count;
	} else { //count <=MINICOUNT
		inverseCountDir ?
				encodercount += PRECISION + count :
				encodercount -= PRECISION + count;
	}
	encodecounton = true;
}
#endif

void refresh_endertostep() {
	endertostep = SPEEDUNIT * speedfactor;
}

void setincrement() {
	if (inverseCountDir) {
		increment = 1;
	} else {
		increment = -1;
	}
}

//change the count dir
void changeDir() {
	inverseCountDir = !inverseCountDir;
	setincrement();
}

//CHNANGE
FORCE_INLINE void counter1_C() {

//	time = millis();
//	interrupts();
//	deltatime = time - lasttime;
//	lasttime = time;

	noInterrupts();
	stateA = READ(ENCODER_A);
//	stateB = digitalRead(ENCODER_B);
//	if (stateA ^ stateB) //state is not same then
//			{
//		encodercount += increment;
//	} else {
//		encodercount -= increment;
//	}
//	stateA ^ stateB ? encodercount += increment : encodercount -= increment;
//	if (inverseCountDir)
//		stateA ^ stateB ? encodercount++ : encodercount--;
//	else
//		stateA ^ stateB ? encodercount-- : encodercount++;

	inverseCountDir ?
			(stateA ^ stateB ? encodercount++ : encodercount--) :
			(stateA ^ stateB ? encodercount-- : encodercount++);

	encodecounton = true;
	interrupts();
}

FORCE_INLINE void counter2_C() {
//
//	time = millis();
//	interrupts();
//	deltatime = time - lasttime;
//	lasttime = time;

	noInterrupts();
//	stateA = digitalRead(ENCODER_A);
	stateB = READ(ENCODER_B);
//	if (!(stateA ^ stateB)) {
//		encodercount += increment;
//	} else {
//		encodercount -= increment;
//	}
//	!(stateA ^ stateB) ? encodercount += increment : encodercount -= increment;

	inverseCountDir ?
			(!(stateA ^ stateB) ? encodercount++ : encodercount--) :
			(!(stateA ^ stateB) ? encodercount-- : encodercount++);

	encodecounton = true;
	interrupts();
}

FORCE_INLINE void counterSub() {

//	time = millis();
//	interrupts();
//	deltatime = time - lasttime;
//	lasttime = time;

	noInterrupts();
	stateA = READ(ENCODER_A);
	stateB = READ(ENCODER_B);
//	if (stateA ^ stateB) //state is not same then
//			{
//		encodercount += increment;
//	} else {
//		encodercount -= increment;
//	}
//	stateA ^ stateB ? encodercount += increment : encodercount -= increment;
	inverseCountDir ?
			(stateA ^ stateB ? encodercount++ : encodercount--) :
			(stateA ^ stateB ? encodercount-- : encodercount++);

	encodecounton = true;
	interrupts();
}
//calculate rotate speed unit step/s use a 100ms timer
//minimus speed which can be detected is encoder/s
//FORCE_INLINE void speedcalculate() {
////allow other interrupt run
//	interrupts();
////	noInterrupts();
//	if (encodecounton) {
//		encodercountfeedrate = (lastencodercount - encodercount);
//		//refresh the lastencodercount
//		lastencodercount = encodercount;
//		absencodercounterfeedrate = abs(encodercountfeedrate) * endertostep;
//		//encoder/s
//	} else {
//		encodercountfeedrate = 0;
//		absencodercounterfeedrate = 0;
//		return;
//	}
//	encodecounton = false;
//
////Test
////	SERIAL_ECHOPGM(" encodercountfeedrate : ");
////	SERIAL_ECHO(encodercountfeedrate);
////	SERIAL_ECHOLNPGM("");
////	interrupts();
//}

void speed_calculate() {
//allow other interrupt run
	interrupts();
//	noInterrupts();
	if (encodecounton) {
		encodercountfeedrate = (lastencodercount - encodercount);
		//refresh the lastencodercount
		lastencodercount = encodercount;
		absencodercounterfeedrate = abs(encodercountfeedrate) * endertostep;
		//encoder/s
	} else {
		encodercountfeedrate = 0;
		absencodercounterfeedrate = 0;
		return;
	}
	encodecounton = false;

//Test
//	SERIAL_ECHOPGM(" encodercountfeedrate : ");
//	SERIAL_ECHO(encodercountfeedrate);
//	SERIAL_ECHOLNPGM("");
//	interrupts();
}

//TODO add positon calculate
//float getPosition() {
//	return float(encodercount / float(ENCODER_COUNTS_PER_UNIT));
//}

long getEncodercount() {
	return encodercount;
}

void setEncodercount(long count) {
	encodercount = count;
}
